<?php

/**
 * @file node.tpl.php

 */
?>


<div class="node<?php if ($sticky):?> <?php print " sticky"; ?><?php endif; ?>
    <?php if (!$status) :?> <?php print " node-unpublished"; ?><?php endif; ?>
    <?php if ($picture) :?> <?php print $picture; ?><?php endif; ?>
    <?php if  ($page == 0):?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php endif; ?>
	<?php if  ($submitted) :?> <?php print $submitted;?><?php endif; ?>
    <div class="taxonomy"><?php print $terms?></div>
    <div class="content"><?php print $content?></div>
     <?php if  ($links) :?> <div class="links">&raquo; <?php print $links?></div><?php endif; ?>
 </div>
