// cookie functions
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { 
        options = options || {};
        if (value === null) {
            value = '';
            options = jQuery.extend({}, options); // clone object since it's unexpected behavior if the expired property were changed
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // NOTE Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { 
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};
// liquid <> fixed
function setPageWidth(){
   var currentWidth = jQuery('#page').css('width');
   if (currentWidth=="95%") newWidth = "960px"; else if (currentWidth=="960px") newWidth = "95%"; else newWidth = "960px";
   jQuery("#page").animate({width: newWidth}, 333).fadeIn("slow");
   jQuery.cookie('pageWidth', newWidth);
}
// body font size
function setFontSize() {
   var size = jQuery.cookie('fontSize');
   if (size=='.8em') newSize = '.95em';
   else if (size=='.95em') newSize = '.7em';
   else if (size=='.7em') newSize = '.8em';
   else newSize = '.95em';
   jQuery("body").animate({fontSize: newSize}, 333).fadeIn("slow");
   jQuery.cookie('fontSize',newSize)
}
jQuery(document).ready(function(){
      // body .safari class
      if (jQuery.browser.safari) jQuery('body').addClass('safari');
      // layout controls
       jQuery("#layoutcontrol a").click(function() {
         switch (jQuery(this).attr("class")) {
    	   case 'setFont':setFontSize();break;
    	   case 'setLiquid':setPageWidth();	break;
    	 }
    	 return false;
        });
       // set the font size from cookie
       var font_size = jQuery.cookie('fontSize');
       if (font_size == '.7em') { jQuery('body').css("font-size",".7em"); }
       if (font_size == '.95em') { jQuery('body').css("font-size",".95em"); }
       if (font_size == '.8em') { jQuery('body').css("font-size",".8em"); }
       // set the page width from cookie
       var page_width = jQuery.cookie('pageWidth');
       if (page_width) jQuery('#page').css('width', page_width);

      jQuery('#secondary-tabs').minitabs(333, 'slide');

      if (document.all && !window.opera && !window.XMLHttpRequest && jQuery.browser.msie) { var isIE6 = true; }
      else { var isIE6 = false;} ;
      jQuery.browser.msie6 = isIE6;
      if (!isIE6) {
        initTooltips({
    		timeout: 6000
       });
      }
      tabmenudropdowns();
});
