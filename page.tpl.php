<?php

/**
 * @page.tpl.php

 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head ?>
<title><?php print $head_title ?></title>
<?php print $styles ?><?php print $scripts ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body class="home">
<div id="page-wrap1">
<div id="page-wrap2">
<!--begin: page wrappers (100% width) -->
<div id="page" class="with-sidebar">
<!-- page (actual site content, custom width) -->
<div id="main-wrap">
<!-- main wrapper (side & main) -->
<div id="mid-wrap">
<!-- mid column wrap -->
<div id="side-wrap">
<!-- sidebar wrap -->
<div id="mid"><!-- mid column -->
  <div id="header"> <!-- header -->
  
  <?php if ($logo) : ?><a id="logo" href="<?php print $front_page ?>" title="<?php print t('Kosova') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Kosova') ?>" /></a><?php endif; ?>
    <div><?php print $header ?></div>
    <div id="tabs"><!--begin: top tab navigation -->
    
    <?php if (isset($primary_links)) : ?><?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')) ?><?php endif; ?>
    </div><!-- end:top tabs --> 
  </div><!-- end: header -->
  
  <div id="mid-content"><!--begin: mid content -->
    
    <?php if ($mission) : ?> <div id="mission"><?php print $mission ?></div><?php endif; ?>
    <div id="main"> <?php print $breadcrumb ?>
      <h1 class="title"><?php print $title ?></h1>
      <div class="tabs"><?php print $tabs ?></div>
        <?php if ($show_messages) : ?> <?php print $messages; ?> <?php endif; ?>
      <?php print $help ?> <?php print $content; ?> <?php print $feed_icons; ?> </div>
  </div>
  <!-- end:mid content --> 
  
</div>
<!-- end:mid -->

<div id="sidebar">
<!--begin: sidebar -->
<div id="sidebar-wrap1">
<!--begin: sidebar 1st container -->

<div id="sidebar-wrap2">
<!--begin: sidebar 2nd container -->
<ul id="sidelist">
<li> 
  <!-- search form --><!-- /search form --> 
</li>
<li>
  <ul class="nav">
    <!-- sidebar menu (categories) --> 
    <!-- left side code-->
     <?php if (isset($secondary_links)) : ?>  <?php print theme('links', $secondary_links, array('class' => 'links', 'id' => 'subnavlist')) ?><?php endif; ?>
   
    <?php print $search_box ?> 
    
    <!-- login --> 
    
    <?php print $sub_right?> 
    
    <!-- /login -->
    
  </ul>
</div>
<!-- end: sidebar 2nd container -->
</div>
<!-- end:sidebar 1st container -->
</div>
<!-- edn:sidebar -->

</div>
<!-- end: side wrap -->
</div>
<!-- end: mid column wrap -->
</div>
<!-- end: main wrapper --> 

<!-- clear main & sidebar sections -->
<div class="clearcontent"></div>
<!-- end: clear main & sidebar sections -->

<div class="footer"><!-- footer --> 
  
  Web Site Designed by <a href="http://www.oxwebs.com/web-developer-in-oxford"  title="Web designer/developer in Oxford UK">Shemsedin Callaki</a> at <a href="http://www.oxwebs.com" >oxWebs</a> | Valid <a href="http://validator.w3.org/check?uri=referer">XHTML 1.0 Strict</a> and <a href="http://bit.ly/9SrddO">CSS 3</a> 
  
  <!--drupal footer--> 
  <?php print $footer_message ?> <?php print $footer ?> <?php print $closure ?> 
  <!--end drupal footer--> 
  
</div>
<!-- end: footer -->

<div id="layoutcontrol"><!-- layout controls --> 
  <a href="javascript:void(0);" class="setFont" title="Increase/Decrease text size"><span>SetTextSize</span></a> <a href="javascript:void(0);" class="setLiquid" title="Switch between full and fixed width"><span>SetPageWidth</span></a> </div>
<!-- end: layout controls -->
</div>
<!-- end:page -->

</div>
</div>
<!-- end: page wrappers -->
</body>
</html>
